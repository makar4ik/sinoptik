import Foundation

enum WeatherMode {
    case current
}

// TODO: add parameters validation

internal class RequestBuilder {
    
    private var token: String?
    var serviceUrl: String
    private var weatherMode: WeatherMode
    private var latitude: Double?
    private var longitude: Double?
    private var city: Int?
    private var countryCode: String?
    
    init() {
        self.serviceUrl = "http://api.openweathermap.org/data/2.5/"
        self.weatherMode = .current
    }
    convenience init (nameCity: String, token: String){
        self.init()
        self.city = nameCityFromCode(city: nameCity)
        self.token = token
        
        }
    func setWeatherMode(mode: WeatherMode) -> RequestBuilder {
        self.weatherMode = mode
        return self
    }
    
    func setLatitude(lat: Double) -> RequestBuilder {
        self.latitude = lat
        return self
    }
    
    func setLongitude(lon: Double) -> RequestBuilder {
        self.longitude = lon
        return self
    }
    
    func setCity(city: Int) -> RequestBuilder {
        self.city = city
        return self
    }
    
    func setCountryCode(code: String?) -> RequestBuilder {
        self.countryCode = code
        return self
    }
    
    func setToken(token: String?) -> RequestBuilder {
        self.token = token
        return self
    }
    
    func build() -> String {
        
        // TODO: add configuration validation
        // TODO: cover by unit test
        
        if weatherMode == .current {
            serviceUrl += "forecast?"
        }
        
        if let city = city {
            serviceUrl += "id=\(city)"
            if let code = countryCode {
                serviceUrl += ",\(code)"
            }
        }
        
        if let latitude = latitude {
            serviceUrl += "lat=\(latitude)"
        }
        
        if let longitude = longitude {
            serviceUrl += "&lon=\(longitude)"
        }
        
        if let token = token {
            serviceUrl += "&appid=\(token)"
        }
        
        #if DEBUG
        print("Generated url: '\(serviceUrl)'")
        #endif
        return serviceUrl
    }
    func nameCityFromCode(city: String) -> Int{
        let pathToFile = Bundle.main.path(forResource: "CityId", ofType: "plist")!
        let dictData = NSDictionary(contentsOfFile: pathToFile) as! [String: Int]
        return dictData[city]!
    }
}

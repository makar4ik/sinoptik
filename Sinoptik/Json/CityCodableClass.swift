import Foundation
import UIKit
import CoreData

struct CityCodableClass: Decodable{
   var city: MyCity
   var list: [MyList]
   
}

struct MyList: Decodable {
    var weather: [MyWeather]
    var main: MyTemperature
    var wind: MyWinds
    var clouds: MyClouds
    var dt_txt: String
}

struct MyCity: Decodable, DataProtocol{
    var name: String
   
    func savePerformance() -> NSManagedObject{
        let cityObject = DataBase(entityName: .City)
        cityObject.object.setValue(self.name, forKey: "name")
        
        return cityObject.object
    }
}

struct MyWeather : Decodable, DataProtocol{
    var id: Int
    var main: String
    var description: String
    var icon: String
    
     func savePerformance() -> NSManagedObject{
        let weatherObject = DataBase(entityName: .WeatherFile)
        weatherObject.object.setValue(self.id, forKey: "id")
        weatherObject.object.setValue(self.main, forKey: "main")
        weatherObject.object.setValue(self.icon, forKey: "temp")
        
        return weatherObject.object
    }
}

struct MyTemperature: Decodable, DataProtocol {
    var temp: Double
    var pressure: Double
    var humidity: Int
    var temp_min: Double
    var temp_max: Double
    
    func savePerformance() -> NSManagedObject{
        let temperatureObject = DataBase(entityName: .TemperatureFile)
        temperatureObject.object.setValue(self.temp, forKey: "temp")
        temperatureObject.object.setValue(self.pressure, forKey: "pressure")
        temperatureObject.object.setValue(self.humidity, forKey: "humidity")
        temperatureObject.object.setValue(self.temp_min, forKey: "temp_min")
        temperatureObject.object.setValue(self.temp_max, forKey: "temp_max")
        
        return temperatureObject.object
    }
}

struct MyClouds: Decodable, DataProtocol{
    var all: Int
    
    func savePerformance() -> NSManagedObject{
        let cloudsObject = DataBase(entityName: .CloudsFile)
        cloudsObject.object.setValue(self.all, forKey: "all")
        
        return cloudsObject.object
    }
}

struct MyWinds: Decodable, DataProtocol {
    var speed: Double
    var deg: Double
    
    func savePerformance() -> NSManagedObject{
        let windsObject = DataBase(entityName: .WindsFile)
        windsObject.object.setValue(self.speed, forKey: "speed")
        windsObject.object.setValue(self.deg, forKey: "deg")
        
        return windsObject.object
    }
}



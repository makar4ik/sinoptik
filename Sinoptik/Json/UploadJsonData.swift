import Foundation

class UploadJsonData {
    
   static let token = "5e9e51b8d7a8f06434500c008a00acf3"
   
   static func uploadJsonAfterSaveCor (name: String){
    
        let urlString = RequestBuilder(nameCity: name, token: UploadJsonData.token)
        urlString.build()
    
        URLSession.shared.dataTask(with: URL(string: urlString.serviceUrl)!) { (data, response, error) in
            if let error = error {
                print(error)
            }
            guard let myData = data else {
                return
            }
            let weatherItem = try? JSONDecoder().decode(CityCodableClass.self, from: myData)
            guard let item = weatherItem else { return}
            
            RealizationsDataBase(data: item).saveData(completionHendler: nil)
        
        }.resume()
    }
    
    static func uploadJson (name: String, completionHendlers: @escaping (_ data: [Days: [MyList]],_ item: CityCodableClass ) -> ()){
        
        let urlString = RequestBuilder(nameCity: name, token: UploadJsonData.token)
        urlString.build()
    
        URLSession.shared.dataTask(with: URL(string: urlString.serviceUrl)!) { (data, response, error) in
            if let error = error {
                print(error)
            }
            guard let myData = data else { return }
            
            let weatherItem = try? JSONDecoder().decode(CityCodableClass.self, from: myData)
            
            guard let item = weatherItem else { return }
            
            let listMasive = item.list
            let sortedListMasive = listMasive.sorted(by: {$0.dt_txt < $1.dt_txt})
           
            var dictMasive: [Days: [MyList]] = [.oneDay:[], .twoDay: [], .threeDay: [], .fourDay: [], .fiveDay: []]
            
            dictMasive[.oneDay]!.append(sortedListMasive[0])
            let initialDay = sortedListMasive[0].dt_txt.toDateTime()
            
            for masivData in sortedListMasive {
                
                switch masivData.dt_txt.toDateTime() {
                case initialDay + 1 :
                    dictMasive[.twoDay]!.append(masivData)
                case initialDay + 2:
                    dictMasive[.threeDay]!.append(masivData)
                case initialDay + 3:
                    dictMasive[.fourDay]!.append(masivData)
                case initialDay + 4:
                    dictMasive[.fiveDay]!.append(masivData)
                default:
                    print()
                }
            }
            DispatchQueue.main.async {
                completionHendlers(dictMasive, item)
        }
         }.resume()
     }
    
    static func checkName (name: String, completionHendlers: @escaping (_ data: String?) -> ()) {
        
        let urlString = RequestBuilder(nameCity: name, token: UploadJsonData.token)
        urlString.build()
        
        URLSession.shared.dataTask(with: URL(string: urlString.serviceUrl)!) { (data, response, error) in
            if let error = error { print(error) }
            guard let myData = data else {
                completionHendlers(nil)
                return
            }
            
            let weatherItem = try? JSONDecoder().decode(CityCodableClass.self, from: myData)
            
            guard let newName = weatherItem else {
                completionHendlers(nil)
                return
            }
            
            completionHendlers(newName.city.name)
            
            }.resume()
        
    }
    
    
    
}

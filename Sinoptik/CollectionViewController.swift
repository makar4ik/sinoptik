import UIKit
import CoreData

struct MinyCity {
    var cityName: String
    var icon: String
    var temperature: String
}

class CollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    private var coreData: [MinyCity] = []
    private var city: MinyCity!
    private let reuseIdentifier = "ColectionCell"
    private let segueForDetal = "SegueForDetal"
    private var selectedIndexPath: IndexPath!
   
    private var triger = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let  longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTap(_:)))
        longPressGesture.minimumPressDuration = TimeInterval(exactly: 1)!
        collectionView.addGestureRecognizer(longPressGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        coreData = RealizationsDataBase().returnDataMiniCity()
        collectionView.reloadData()
    }
    
    @objc func longTap(_ gesture: UIGestureRecognizer){
        
        switch(gesture.state) {
        case .began:
             selectedIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView))
             remove(index: selectedIndexPath!.row)
        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            collectionView.endInteractiveMovement()
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
    
    func remove (index: Int ){
        RealizationsDataBase().deleteData(name: coreData[index].cityName)
        
        coreData.remove(at: index)
        
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.performBatchUpdates({
            self.collectionView.deleteItems(at: [indexPath])
            }, completion: {(finish) in
                self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
                })
    }
    
    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coreData.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        if !coreData.isEmpty{

        cell.cityNameCell.text = coreData[indexPath.row].cityName
        cell.temperatureCell.text = coreData[indexPath.row].temperature
        cell.immageCell.image = UIImage(named: coreData[indexPath.row].icon)
        cell.backgroundColor = UIColor(displayP3Red: 31/255, green: 36/255, blue: 39/255, alpha: 1)
        }
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        city = coreData[indexPath.row]
        performSegue(withIdentifier: segueForDetal, sender: self)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueForDetal{
            let  distanViewController = segue.destination as! DetalTabViewController
            let vc = distanViewController.viewControllers![0] as! OneViewController
            vc.name = city.cityName
        }
    }
}

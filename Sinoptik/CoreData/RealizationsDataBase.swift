import Foundation
import CoreData

enum Days{
    case oneDay
    case twoDay
    case threeDay
    case fourDay
    case fiveDay
}
typealias Hendler  = ()->()

class RealizationsDataBase {
    private var  dataSaveJeason: CityCodableClass!
    private let  request : NSFetchRequest <CityClass>  = CityClass.fetchRequest()
    
    func saveData (completionHendler: Hendler?){
      
        request.predicate = NSPredicate(format: "myName == %@", dataSaveJeason.city.name)
        request.returnsObjectsAsFaults = false
        do {
            let result = try? DataBase.emptyContext().fetch(request)
               if  let dataObj = result {
                  for usrObj in dataObj {
                     DataBase.emptyContext().delete(usrObj)
            }
          }
        } catch {
            print("Failed")
        }
        
        let cityObject = dataSaveJeason.city.savePerformance()
        let cityClass = DataBase(entityName: .CityClass)
        cityClass.object.setValue(cityObject, forKey: "city")
        cityClass.object.setValue(dataSaveJeason.city.name, forKey: "myName")
        
        for listData in dataSaveJeason.list {
            let windsObject = listData.wind.savePerformance()
            let cloudsObject = listData.clouds.savePerformance()
            let mainObject = listData.main.savePerformance()
            let listClass = DataBase(entityName: .List)
            
            for weatherData in listData.weather{
                let weatherObject = DataBase(entityName: .WeatherFile)
                weatherObject.object.setValue(weatherData.icon, forKey: "icon")
                weatherObject.object.setValue(weatherData.id, forKey: "id")
                weatherObject.object.setValue(weatherData.main, forKey: "main")
                weatherObject.object.setValue(listClass.object, forKey: "list")
            }
        
            listClass.object.setValue(windsObject, forKey: "winds")
            listClass.object.setValue(cloudsObject, forKey: "cloud")
            listClass.object.setValue(mainObject, forKey: "temperature")
            listClass.object.setValue(listData.dt_txt, forKey: "dt_txt")
            listClass.object.setValue(cityClass.object, forKey: "mycity")
        }
        
        do {
            try DataBase.emptyContext().save()
            guard let hendler =  completionHendler else {return}
                hendler()
            } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            }
        }
    
    func returnData (name: String) -> [CityClass]{
        var result: [CityClass]?
        request.predicate = NSPredicate(format: "myName == %@", name)
        request.returnsObjectsAsFaults = false
        do {
            result = try DataBase.emptyContext().fetch(request)
        } catch {
          print("Failed")
        }
        guard let data = result else {
            return []
        }
        return data
    }
    
    func returnDataCity (name: String) -> [Days: [List]]{
    
        var result: [CityClass]?
        request.predicate = NSPredicate(format: "myName == %@", name)
        request.returnsObjectsAsFaults = false
        do {
            result = try DataBase.emptyContext().fetch(request)
        } catch {
            print("Failed")
        }
        guard let data = result else {
            return [:]
        }
            let listMasive = data[0].list?.allObjects as! [List]
            let sortedListMasive = listMasive.sorted(by: {$0.dt_txt! < $1.dt_txt!})
            var dictMasive: [Days: [List]] = [.oneDay:[], .twoDay: [], .threeDay: [], .fourDay: [], .fiveDay: []]
            dictMasive[.oneDay]!.append(sortedListMasive[0])
            let initialDay = sortedListMasive[0].dt_txt!.toDateTime()

            for masivData in sortedListMasive {
                
                switch masivData.dt_txt!.toDateTime() {
                case initialDay + 1 :
                    dictMasive[.twoDay]!.append(masivData)
                case initialDay + 2:
                    dictMasive[.threeDay]!.append(masivData)
                case initialDay + 3:
                    dictMasive[.fourDay]!.append(masivData)
                case initialDay + 4:
                    dictMasive[.fiveDay]!.append(masivData)
                default:
                    print()
                }
        }
        return dictMasive
    }

    func returnDataMiniCity () -> [MinyCity]{
        var result: [CityClass]?
        request.returnsObjectsAsFaults = false
        var coreData: [MinyCity] = []
        do {
            result = try? DataBase.emptyContext().fetch(request)
        } catch {
            print("Failed")
        }
        guard let data = result else {
            return []
        }
        for mydata in data {
            let listMasive = mydata.list?.allObjects as! [List]
            let sortedListMasive = listMasive.sorted(by: {$0.dt_txt! < $1.dt_txt!})
            let weatherMasive = sortedListMasive[0].weather?.allObjects as! [WeatherFile]

            let minyCityName = mydata.myName!
            let minyCityTemperature = sortedListMasive[0].temperature!.temp.fahrenheitForCelsius()
            let minyCityIcon = weatherMasive[0].icon!

            coreData.insert(MinyCity(cityName: minyCityName, icon: minyCityIcon, temperature: minyCityTemperature), at: 0)
       }
        return coreData
    }
    
    func deleteData (name:String){
        request.predicate = NSPredicate(format: "myName == %@", name)
        request.returnsObjectsAsFaults = false
        do {
            let result = try? DataBase.emptyContext().fetch(request)
            if  let dataObj = result {
                for usrObj in dataObj {
                    DataBase.emptyContext().delete(usrObj)
                }
            }
            try DataBase.emptyContext().save()
            
        } catch let error as Error {
            print(error)
        }
    }
    
    func returnMasive() -> Key?{
        let personName = "Max"
        let fetchRequest: NSFetchRequest<Key> = Key.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nameKey == %@", personName)
        var key: Key!
        
        do {
            let results = try DataBase.emptyContext().fetch(fetchRequest)
            if results.isEmpty {
                key = Key(context: DataBase.emptyContext())
                key.nameKey = personName
                try DataBase.emptyContext().save()
            } else {
                key = results.first
            }
        } catch let error as NSError{
            print(error.userInfo)
      }
        return key
    }
 
    func saveKey(name:String){
        let val = Value(context: DataBase.emptyContext())
        val.nameMyCity = name
        
        guard let data = RealizationsDataBase().returnMasive() else { return }
        
        let newValue = data.value?.mutableCopy() as? NSMutableOrderedSet
        newValue?.add(val)
        data.value = newValue
        
     do {
        try DataBase.emptyContext().save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    init(){ }
    
    init(data: CityCodableClass) {
        self.dataSaveJeason = data
    }
}

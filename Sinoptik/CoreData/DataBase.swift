import UIKit
import CoreData

enum DataType: String {
    case City = "City"
    case CityClass = "CityClass"
    case CloudsFile = "CloudsFile"
    case TemperatureFile = "TemperatureFile"
    case WindsFile = "WindsFile"
    case WeatherFile = "WeatherFile"
    case List = "List"
}
protocol DataProtocol {
    func savePerformance() -> NSManagedObject
    }

class DataBase {
    var object: NSManagedObject!
    var entityName: DataType
    private static var context: NSManagedObjectContext?
    
    static  func emptyContext() -> NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
 
        if DataBase.context == nil{
             context = appDelegate!.persistentContainer.viewContext
        }
        return context!
       
    }
    
    init (entityName: DataType){
        self.entityName = entityName
        let entity = NSEntityDescription.entity(forEntityName: "\(entityName)", in: DataBase.emptyContext())
        let neWObject = NSManagedObject(entity: entity!, insertInto: DataBase.emptyContext())
        self.object = neWObject
    }

}


import Foundation
import UIKit

// MARK: Extension
extension Double {
    func fahrenheitForCelsius() -> String {
        return String(Int(self - 273)) + "°C"
    }
}

extension String{
    func toDateTime() -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormater.date(from: self)
        let calendar = Calendar.current
        let components = calendar.component(.day, from: date!)
        return components
    }
    func dataFromWeak() -> String? {  ////NAw func
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormater.date(from: self)
        
        return date?.dayNumberOfWeek()
    }
    func winds() -> String {
        return self + " m/s"
    }
    
}
extension Int {
    func intToStringPlusProcent() -> String{
        return String(self) + " %"
    }
    func intToStringPlusCelsiys() -> String{
        return String(self) + "°C"
    }
}
enum Weak: String , CaseIterable{
    case Sunday
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Satudrday
}
extension Date {
    func dayNumberOfWeek() -> String?{
        let components = Calendar.current.dateComponents([.weekday], from: self).weekday
        guard let dey = components else { return nil }
        
        return Weak.allCases[dey - 1].rawValue
    }
}

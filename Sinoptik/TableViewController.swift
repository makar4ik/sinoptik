import UIKit
import OpenWeatherMapKit
import CoreData

class TableViewController: UITableViewController{
    private var city: String!
    private var data: Key!
    private var searchController: UISearchController!
    private var cityWeather : CityCodableClass!

    override func viewDidLoad() {
        super.viewDidLoad()
        data = RealizationsDataBase().returnMasive()
        initionalSearchConroller()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let myData = data.value else { return 1 }
            return myData.count
        }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let myData = data.value?.array[indexPath.row] as? Value, let cityName = myData.nameMyCity as String? else { return }
        city = cityName
        
        performSegue(withIdentifier: "detalSegue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
    
        guard let myData = data.value?.array[indexPath.row] as? Value, let cityName = myData.nameMyCity as String? else { return cell }
        
        cell.textLabel?.text = cityName
        cell.textLabel?.textColor = UIColor.white
        
        return cell
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard let myData = data.value?.array[indexPath.row] as? Value,editingStyle == .delete else { return }
    
        DataBase.emptyContext().delete(myData)
        
        do {
            try  DataBase.emptyContext().save()
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } catch let error as NSError {
            print("Error: \(error), description \(error.userInfo)")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let  distanViewController = segue.destination as! DetalTabViewController
            let vc1 = distanViewController.viewControllers![0] as! OneViewController
            vc1.name = city
          
    }
    
    private func initionalSearchConroller (){
        let resultController = SearchResultController()
        searchController = UISearchController(searchResultsController: resultController)
        let searchBar = searchController.searchBar
        searchBar.placeholder = "Enter a search city"
        searchBar.sizeToFit()
        searchBar.barTintColor = UIColor(displayP3Red: 31/255, green: 36/255, blue: 39/255, alpha: 1)
        searchBar.tintColor = UIColor.white
        tableView.tableHeaderView = searchBar
        searchController.searchResultsUpdater = resultController
        searchBar.delegate = self
        definesPresentationContext = true
    }
}

  // MARK: - Extension add protokol UISearchBarDelegate

extension TableViewController: UISearchBarDelegate{
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
         tableView.reloadData()
    }
}


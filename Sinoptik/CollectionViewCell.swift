import UIKit
import CoreData
class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var immageCell: UIImageView!
    @IBOutlet weak var temperatureCell: UILabel!
    @IBOutlet weak var cityNameCell: UILabel!
    
}

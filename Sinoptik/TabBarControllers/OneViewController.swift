import UIKit
import CoreData

class OneViewController: UIViewController {
    var name: String!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tempCity: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var speedWeater: UILabel!
    @IBOutlet weak var humidity: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cityName.text = name
        
        let activityIndicatorView = UIActivityIndicatorView(style:.gray)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.frame = self.view.frame
        activityIndicatorView.center = self.view.center
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.startAnimating()
        view.alpha = 0.1
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        UploadJsonData.uploadJson(name: name) { data, item in
            let optionsData = data[.oneDay]!

            if !optionsData.isEmpty{
                let dayData = optionsData[0]

                self.tempCity.text! = String(dayData.main.temp.fahrenheitForCelsius())
                self.humidity.text!  = Int(dayData.main.humidity).intToStringPlusProcent()
                self.speedWeater.text! = String(dayData.wind.speed).winds()
                let weaterMasive = dayData.weather
                self.imageWeather.image = UIImage(named: weaterMasive[0].icon)
                
                RealizationsDataBase(data: item).saveData{
                     activityIndicatorView.stopAnimating()
                     self.view.alpha = 1
                    
                     UIApplication.shared.endIgnoringInteractionEvents()
            }
          }
       }
    }
}

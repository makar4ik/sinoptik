import UIKit

class ThreeViewController: UIViewController {
    var name: String {
        get {
            return (self.tabBarController!.viewControllers![0] as! OneViewController).name
        }
    }
    
    @IBOutlet weak var tempCity: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var speedWeater: UILabel!
    @IBOutlet weak var humidity: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cityName.text = name
        
        let data = RealizationsDataBase().returnDataCity(name: name)
        let optionsData = data[.threeDay]!
        
        if !optionsData.isEmpty{
            let dayData = optionsData[3]
         
            tempCity.text! = String(dayData.temperature!.temp.fahrenheitForCelsius())
            humidity.text!  = Int(dayData.temperature!.humidity).intToStringPlusProcent()
            speedWeater.text! = String(dayData.winds!.speed).winds()
            
            let weaterMasive = dayData.weather?.allObjects as! [WeatherFile]
            imageWeather.image = UIImage(named: weaterMasive[0].icon!)
        }
    }
}

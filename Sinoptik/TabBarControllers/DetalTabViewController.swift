import UIKit

class DetalTabViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let controllers =  self.viewControllers else { return }
        let toDay = Date()
            for i in 0...4{
                let date = Calendar.current.date(byAdding: .day, value: i, to: toDay)
                controllers[i].tabBarItem.title = date?.dayNumberOfWeek()
        }
    }
}

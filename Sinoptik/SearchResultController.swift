import UIKit
import CoreData

class SearchResultController: UITableViewController, UISearchResultsUpdating {
    private var dictData: [String:[String]]!
    private var keyMasive: [String]!
    private var filterNames: [String] = []
    private let sectionTableCell = "Cell"
    private var searchBool = false
    private weak var searchController: UISearchController!
    
    func updateSearchResults(for searchController: UISearchController) {
        self.searchController = searchController
        let searchString = searchController.searchBar.text!
        filterNames.removeAll(keepingCapacity: true)
        searchBool = true
        if !searchString.isEmpty {
            let filter : (Any) -> Bool = { name in
                guard let nameString = name as? String else{
                    return false
                }
                let range = nameString.range(of: searchString)
                return range != nil
            }
            
            for key in keyMasive{
                let nameForKey = dictData[key]!
                let matches = nameForKey.filter(filter)
                filterNames += matches
            }
        }else {
            // searchBool = false
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor(displayP3Red: 31/255, green: 36/255, blue: 39/255, alpha: 1)
        parsePlist()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: sectionTableCell)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchBool == false {
            return keyMasive.count
        }else{
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBool == false {
            let nameKey = keyMasive[section]
            let nameCity = dictData[nameKey]!
            return nameCity.count
        }else{
            return filterNames.count
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchBool == false {
            return keyMasive[section]
        }else{
            return  nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: sectionTableCell, for: indexPath)
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = UIColor(displayP3Red: 31/255, green: 36/255, blue: 39/255, alpha: 1)
        if searchBool == false {
            let nameKeys = keyMasive[indexPath.section]
            let nameCity = dictData[nameKeys]!
            cell.textLabel?.text = nameCity[indexPath.row]
        }else {
            cell.textLabel?.text = filterNames[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        RealizationsDataBase().saveKey(name:filterNames[indexPath.row])
        searchController.isActive = false
    }
    
    func parsePlist(){
        let pathToFile = Bundle.main.path(forResource: "CityNamed", ofType: "plist")!
        dictData = NSDictionary(contentsOfFile: pathToFile) as! [String:[String]]
        keyMasive = dictData.keys.sorted{$0 < $1}
    }
}

